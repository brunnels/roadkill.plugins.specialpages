﻿namespace Roadkill.Plugins.SpecialPages
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Web.Mvc;
  using System.Web.Routing;

  using Roadkill.Core.Mvc.Controllers;
  using Roadkill.Core.Mvc.ViewModels;
  using Roadkill.Core.Plugins;

  /// <summary>
  ///   A special url that redirects to a random page in the wiki.
  /// </summary>
  public class RandomPage2 : SpecialPagePlugin
  {
    #region Static Fields

    private static Random _random = new Random();

    #endregion

    #region Constructors and Destructors

    public RandomPage2()
    {
    }

    internal RandomPage2(Random random)
    {
      _random = random;
    }

    #endregion

    #region Public Properties

    public override string Name
    {
      get
      {
        return "Random2";
      }
    }

    #endregion

    #region Public Methods and Operators

    public override ActionResult GetResult(SpecialPagesController controller)
    {
      RouteValueDictionary routeValueDictionary = new RouteValueDictionary();
      List<PageViewModel> pages = this.PageService.AllPages().ToList();

      if (pages.Count == 0)
      {
        routeValueDictionary.Add("controller", "Home");
        routeValueDictionary.Add("action", "Index");
      }
      else
      {
        int randomIndex = _random.Next(0, pages.Count - 1);
        PageViewModel randomPage = pages[randomIndex];

        routeValueDictionary.Add("controller", "Page");
        routeValueDictionary.Add("action", "Index");
        routeValueDictionary.Add("id", randomPage.Id);
      }

      return new RedirectToRouteResult(routeValueDictionary);
    }

    #endregion
  }
}